# Angular9

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.0-next.5.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


```
$ time ng new angular9
? Would you like to add Angular routing? Yes
? Which stylesheet format would you like to use? Sass   [ https://sass-lang.com/documentation/syntax#the-indented-syntax ]
CREATE angular9/README.md (1032 bytes)
CREATE angular9/.editorconfig (246 bytes)
CREATE angular9/.gitignore (631 bytes)
CREATE angular9/angular.json (3669 bytes)
CREATE angular9/package.json (1367 bytes)
CREATE angular9/tsconfig.json (543 bytes)
CREATE angular9/tslint.json (1988 bytes)
CREATE angular9/browserslist (429 bytes)
CREATE angular9/karma.conf.js (1020 bytes)
CREATE angular9/tsconfig.app.json (210 bytes)
CREATE angular9/tsconfig.spec.json (270 bytes)
CREATE angular9/src/favicon.ico (948 bytes)
CREATE angular9/src/index.html (294 bytes)
CREATE angular9/src/main.ts (372 bytes)
CREATE angular9/src/polyfills.ts (2838 bytes)
CREATE angular9/src/styles.sass (80 bytes)
CREATE angular9/src/test.ts (642 bytes)
CREATE angular9/src/assets/.gitkeep (0 bytes)
CREATE angular9/src/environments/environment.prod.ts (51 bytes)
CREATE angular9/src/environments/environment.ts (662 bytes)
CREATE angular9/src/app/app-routing.module.ts (246 bytes)
CREATE angular9/src/app/app.module.ts (393 bytes)
CREATE angular9/src/app/app.component.sass (0 bytes)
CREATE angular9/src/app/app.component.html (25692 bytes)
CREATE angular9/src/app/app.component.spec.ts (1104 bytes)
CREATE angular9/src/app/app.component.ts (213 bytes)
CREATE angular9/e2e/protractor.conf.js (810 bytes)
CREATE angular9/e2e/tsconfig.json (214 bytes)
CREATE angular9/e2e/src/app.e2e-spec.ts (641 bytes)
CREATE angular9/e2e/src/app.po.ts (262 bytes)
warning @angular-devkit/build-angular@0.900.0-next.5: The engine "pnpm" appears to be invalid.
warning @angular-devkit/architect@0.900.0-next.5: The engine "pnpm" appears to be invalid.
warning @angular-devkit/core@9.0.0-next.5: The engine "pnpm" appears to be invalid.
warning @angular-devkit/build-webpack@0.900.0-next.5: The engine "pnpm" appears to be invalid.
warning @ngtools/webpack@9.0.0-next.5: The engine "pnpm" appears to be invalid.
warning @angular-devkit/build-optimizer@0.900.0-next.5: The engine "pnpm" appears to be invalid.
warning @angular/cli@9.0.0-next.5: The engine "pnpm" appears to be invalid.
warning @schematics/angular@9.0.0-next.5: The engine "pnpm" appears to be invalid.
warning @schematics/update@0.900.0-next.5: The engine "pnpm" appears to be invalid.
warning @angular-devkit/schematics@9.0.0-next.5: The engine "pnpm" appears to be invalid.
warning " > @angular-devkit/build-angular@0.900.0-next.5" has incorrect peer dependency "@angular/compiler-cli@^8.0.0-beta.0 || ^8.1.0-beta.0 || ^8.2.0-beta.0 || ^8.3.0-beta.0 || ^8.4.0-beta.0 || >=9.0.0-beta < 9".
warning "@angular-devkit/build-angular > @ngtools/webpack@9.0.0-next.5" has incorrect peer dependency "@angular/compiler-cli@^8.0.0-beta.0 || ^8.1.0-beta.0 || ^8.2.0-beta.0 || ^8.3.0-beta.0 || ^8.4.0-beta.0 || >=9.0.0-beta < 9".
    Successfully initialized git.

real    1m36.975s
user    0m27.716s
sys     0m10.677s



```


```
$ ng --version

     _                      _                 ____ _     ___
    / \   _ __   __ _ _   _| | __ _ _ __     / ___| |   |_ _|
   / △ \ | '_ \ / _` | | | | |/ _` | '__|   | |   | |    | |
  / ___ \| | | | (_| | |_| | | (_| | |      | |___| |___ | |
 /_/   \_\_| |_|\__, |\__,_|_|\__,_|_|       \____|_____|___|
                |___/


Angular CLI: 9.0.0-next.5
Node: 10.16.3
OS: linux x64
Angular: 9.0.0-next.7
... animations, common, compiler, compiler-cli, core, forms
... language-service, platform-browser, platform-browser-dynamic
... router

Package                           Version
-----------------------------------------------------------
@angular-devkit/architect         0.900.0-next.5
@angular-devkit/build-angular     0.900.0-next.5
@angular-devkit/build-optimizer   0.900.0-next.5
@angular-devkit/build-webpack     0.900.0-next.5
@angular-devkit/core              9.0.0-next.5
@angular-devkit/schematics        9.0.0-next.5
@angular/cli                      9.0.0-next.5
@ngtools/webpack                  9.0.0-next.5
@schematics/angular               9.0.0-next.5
@schematics/update                0.900.0-next.5
rxjs                              6.4.0
typescript                        3.5.3
webpack                           4.40.2
```

```
$ time ng add @angular/material
Installing packages for tooling via yarn.
yarn add v1.17.3
[1/4] Resolving packages...
[2/4] Fetching packages...
warning @angular-devkit/build-angular@0.900.0-next.5: The engine "pnpm" appears to be invalid.
warning @angular-devkit/architect@0.900.0-next.5: The engine "pnpm" appears to be invalid.
warning @angular-devkit/build-optimizer@0.900.0-next.5: The engine "pnpm" appears to be invalid.
warning @angular-devkit/build-webpack@0.900.0-next.5: The engine "pnpm" appears to be invalid.
warning @angular-devkit/core@9.0.0-next.5: The engine "pnpm" appears to be invalid.
warning @ngtools/webpack@9.0.0-next.5: The engine "pnpm" appears to be invalid.
info fsevents@2.0.7: The platform "linux" is incompatible with this module.
info "fsevents@2.0.7" is an optional dependency and failed compatibility check. Excluding it from installation.
info fsevents@1.2.9: The platform "linux" is incompatible with this module.
info "fsevents@1.2.9" is an optional dependency and failed compatibility check. Excluding it from installation.
warning @angular/cli@9.0.0-next.5: The engine "pnpm" appears to be invalid.
warning @angular-devkit/schematics@9.0.0-next.5: The engine "pnpm" appears to be invalid.
warning @schematics/angular@9.0.0-next.5: The engine "pnpm" appears to be invalid.
warning @schematics/update@0.900.0-next.5: The engine "pnpm" appears to be invalid.
[3/4] Linking dependencies...
warning " > @angular-devkit/build-angular@0.900.0-next.5" has incorrect peer dependency "@angular/compiler-cli@^8.0.0-beta.0 || ^8.1.0-beta.0 || ^8.2.0-beta.0 || ^8.3.0-beta.0 || ^8.4.0-beta.0 || >=9.0.0-beta < 9".
warning "@angular-devkit/build-angular > @ngtools/webpack@9.0.0-next.5" has incorrect peer dependency "@angular/compiler-cli@^8.0.0-beta.0 || ^8.1.0-beta.0 || ^8.2.0-beta.0 || ^8.3.0-beta.0 || ^8.4.0-beta.0 || >=9.0.0-beta < 9".
warning " > codelyzer@5.1.1" has incorrect peer dependency "@angular/compiler@>=2.3.1 <9.0.0 || >8.0.0-beta <9.0.0 || >8.1.0-beta <9.0.0 || >8.2.0-beta <9.0.0".
warning " > codelyzer@5.1.1" has incorrect peer dependency "@angular/core@>=2.3.1 <9.0.0 || >8.0.0-beta <9.0.0 || >8.1.0-beta <9.0.0 || >8.2.0-beta <9.0.0".
warning " > @angular/material@8.2.0" has unmet peer dependency "@angular/cdk@8.2.0".
[4/4] Building fresh packages...
success Saved lockfile.
success Saved 1 new dependency.
info Direct dependencies
└─ @angular/material@8.2.0
info All dependencies
└─ @angular/material@8.2.0
Done in 13.03s.
Installed packages for tooling via yarn.
? Choose a prebuilt theme name, or "custom" for a custom theme: Custom
? Set up HammerJS for gesture recognition? Yes
? Set up browser animations for Angular Material? Yes
UPDATE package.json (1457 bytes)
warning @angular-devkit/build-angular@0.900.0-next.5: The engine "pnpm" appears to be invalid.
warning @angular-devkit/architect@0.900.0-next.5: The engine "pnpm" appears to be invalid.
warning @angular-devkit/build-optimizer@0.900.0-next.5: The engine "pnpm" appears to be invalid.
warning @angular-devkit/build-webpack@0.900.0-next.5: The engine "pnpm" appears to be invalid.
warning @angular-devkit/core@9.0.0-next.5: The engine "pnpm" appears to be invalid.
warning @ngtools/webpack@9.0.0-next.5: The engine "pnpm" appears to be invalid.
warning @angular/cli@9.0.0-next.5: The engine "pnpm" appears to be invalid.
warning @angular-devkit/schematics@9.0.0-next.5: The engine "pnpm" appears to be invalid.
warning @schematics/angular@9.0.0-next.5: The engine "pnpm" appears to be invalid.
warning @schematics/update@0.900.0-next.5: The engine "pnpm" appears to be invalid.
warning " > @angular-devkit/build-angular@0.900.0-next.5" has incorrect peer dependency "@angular/compiler-cli@^8.0.0-beta.0 || ^8.1.0-beta.0 || ^8.2.0-beta.0 || ^8.3.0-beta.0 || ^8.4.0-beta.0 || >=9.0.0-beta < 9".
warning "@angular-devkit/build-angular > @ngtools/webpack@9.0.0-next.5" has incorrect peer dependency "@angular/compiler-cli@^8.0.0-beta.0 || ^8.1.0-beta.0 || ^8.2.0-beta.0 || ^8.3.0-beta.0 || ^8.4.0-beta.0 || >=9.0.0-beta < 9".
warning " > codelyzer@5.1.1" has incorrect peer dependency "@angular/compiler@>=2.3.1 <9.0.0 || >8.0.0-beta <9.0.0 || >8.1.0-beta <9.0.0 || >8.2.0-beta <9.0.0".
warning " > codelyzer@5.1.1" has incorrect peer dependency "@angular/core@>=2.3.1 <9.0.0 || >8.0.0-beta <9.0.0 || >8.1.0-beta <9.0.0 || >8.2.0-beta <9.0.0".
CREATE src/custom-theme.scss (1378 bytes)
UPDATE src/main.ts (391 bytes)
UPDATE src/app/app.module.ts (502 bytes)
UPDATE angular.json (3743 bytes)
UPDATE src/index.html (488 bytes)

real    0m40.805s
user    0m22.934s
sys     0m6.097s
```

```
$ time ng add @angular/pwa
Installing packages for tooling via yarn.
yarn add v1.17.3
[1/4] Resolving packages...
[2/4] Fetching packages...
warning @angular-devkit/build-angular@0.900.0-next.5: The engine "pnpm" appears to be invalid.
warning @angular-devkit/architect@0.900.0-next.5: The engine "pnpm" appears to be invalid.
warning @angular-devkit/build-optimizer@0.900.0-next.5: The engine "pnpm" appears to be invalid.
warning @angular-devkit/build-webpack@0.900.0-next.5: The engine "pnpm" appears to be invalid.
warning @angular-devkit/core@9.0.0-next.5: The engine "pnpm" appears to be invalid.
warning @ngtools/webpack@9.0.0-next.5: The engine "pnpm" appears to be invalid.
info fsevents@2.0.7: The platform "linux" is incompatible with this module.
info "fsevents@2.0.7" is an optional dependency and failed compatibility check. Excluding it from installation.
info fsevents@1.2.9: The platform "linux" is incompatible with this module.
info "fsevents@1.2.9" is an optional dependency and failed compatibility check. Excluding it from installation.
warning @angular/cli@9.0.0-next.5: The engine "pnpm" appears to be invalid.
warning @angular-devkit/schematics@9.0.0-next.5: The engine "pnpm" appears to be invalid.
warning @schematics/angular@9.0.0-next.5: The engine "pnpm" appears to be invalid.
warning @schematics/update@0.900.0-next.5: The engine "pnpm" appears to be invalid.
[3/4] Linking dependencies...
warning " > @angular-devkit/build-angular@0.900.0-next.5" has incorrect peer dependency "@angular/compiler-cli@^8.0.0-beta.0 || ^8.1.0-beta.0 || ^8.2.0-beta.0 || ^8.3.0-beta.0 || ^8.4.0-beta.0 || >=9.0.0-beta < 9".
warning "@angular-devkit/build-angular > @ngtools/webpack@9.0.0-next.5" has incorrect peer dependency "@angular/compiler-cli@^8.0.0-beta.0 || ^8.1.0-beta.0 || ^8.2.0-beta.0 || ^8.3.0-beta.0 || ^8.4.0-beta.0 || >=9.0.0-beta < 9".
warning " > codelyzer@5.1.1" has incorrect peer dependency "@angular/compiler@>=2.3.1 <9.0.0 || >8.0.0-beta <9.0.0 || >8.1.0-beta <9.0.0 || >8.2.0-beta <9.0.0".
warning " > codelyzer@5.1.1" has incorrect peer dependency "@angular/core@>=2.3.1 <9.0.0 || >8.0.0-beta <9.0.0 || >8.1.0-beta <9.0.0 || >8.2.0-beta <9.0.0".
[4/4] Building fresh packages...
success Saved lockfile.
success Saved 4 new dependencies.
info Direct dependencies
└─ @angular/pwa@0.803.5
info All dependencies
├─ @angular/pwa@0.803.5
├─ @schematics/angular@8.3.5
├─ parse5-html-rewriting-stream@5.1.0
└─ parse5-sax-parser@5.1.0
Done in 8.62s.
Installed packages for tooling via yarn.
CREATE ngsw-config.json (620 bytes)
CREATE src/manifest.webmanifest (1073 bytes)
CREATE src/assets/icons/icon-128x128.png (1253 bytes)
CREATE src/assets/icons/icon-144x144.png (1394 bytes)
CREATE src/assets/icons/icon-152x152.png (1427 bytes)
CREATE src/assets/icons/icon-192x192.png (1790 bytes)
CREATE src/assets/icons/icon-384x384.png (3557 bytes)
CREATE src/assets/icons/icon-512x512.png (5008 bytes)
CREATE src/assets/icons/icon-72x72.png (792 bytes)
CREATE src/assets/icons/icon-96x96.png (958 bytes)
UPDATE angular.json (3916 bytes)
UPDATE package.json (1538 bytes)
UPDATE src/app/app.module.ts (713 bytes)
UPDATE src/index.html (674 bytes)
warning @angular-devkit/build-angular@0.900.0-next.5: The engine "pnpm" appears to be invalid.
warning @angular-devkit/architect@0.900.0-next.5: The engine "pnpm" appears to be invalid.
warning @angular-devkit/build-optimizer@0.900.0-next.5: The engine "pnpm" appears to be invalid.
warning @angular-devkit/build-webpack@0.900.0-next.5: The engine "pnpm" appears to be invalid.
warning @angular-devkit/core@9.0.0-next.5: The engine "pnpm" appears to be invalid.
warning @ngtools/webpack@9.0.0-next.5: The engine "pnpm" appears to be invalid.
warning @angular/cli@9.0.0-next.5: The engine "pnpm" appears to be invalid.
warning @angular-devkit/schematics@9.0.0-next.5: The engine "pnpm" appears to be invalid.
warning @schematics/angular@9.0.0-next.5: The engine "pnpm" appears to be invalid.
warning @schematics/update@0.900.0-next.5: The engine "pnpm" appears to be invalid.
warning " > @angular-devkit/build-angular@0.900.0-next.5" has incorrect peer dependency "@angular/compiler-cli@^8.0.0-beta.0 || ^8.1.0-beta.0 || ^8.2.0-beta.0 || ^8.3.0-beta.0 || ^8.4.0-beta.0 || >=9.0.0-beta < 9".
warning "@angular-devkit/build-angular > @ngtools/webpack@9.0.0-next.5" has incorrect peer dependency "@angular/compiler-cli@^8.0.0-beta.0 || ^8.1.0-beta.0 || ^8.2.0-beta.0 || ^8.3.0-beta.0 || ^8.4.0-beta.0 || >=9.0.0-beta < 9".
warning " > codelyzer@5.1.1" has incorrect peer dependency "@angular/compiler@>=2.3.1 <9.0.0 || >8.0.0-beta <9.0.0 || >8.1.0-beta <9.0.0 || >8.2.0-beta <9.0.0".
warning " > codelyzer@5.1.1" has incorrect peer dependency "@angular/core@>=2.3.1 <9.0.0 || >8.0.0-beta <9.0.0 || >8.1.0-beta <9.0.0 || >8.2.0-beta <9.0.0".

real    0m18.407s
user    0m20.560s
sys     0m5.166s
```


